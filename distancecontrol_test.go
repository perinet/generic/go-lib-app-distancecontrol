// Copyright (c) 2018-2022 Perinet GmbH
// All rights reserved

// This software is dual-licensed: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License version 3 as
// published by the Free Software Foundation. For the terms of this
// license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
// or contact us at https://perinet.io/contact/ when you want license
// this software under a commercial license.

package appDistanceControl

import (
	"fmt"
	"testing"

	"github.com/tidwall/gjson"
	"gitlab.com/perinet/generic/apiservice/mqttclient"
	"gotest.tools/v3/assert"
)

func TestDistances(t *testing.T) {

	setpoint := uint32(100)
	contactorChan := make(chan string, 1)
	distancesChan := make(chan mqttclient.MQTTClientSubscriberData, 1)
	assert.Assert(t, hysteresis > 1)
	dC := NewDistanceControl(setpoint, contactorChan, distancesChan)

	var message string
	var value bool

	distancesChan <- mqttclient.MQTTClientSubscriberData{Topic: "test", Message: `{"data": {"value":` + fmt.Sprint(setpoint-(hysteresis+1)) + `}}`}
	message = <-contactorChan
	value = gjson.Get(message, "data").Bool()
	assert.Assert(t, value == true)

	distancesChan <- mqttclient.MQTTClientSubscriberData{Topic: "test", Message: `{"data": {"value":` + fmt.Sprint(setpoint) + `}}`}
	message = <-contactorChan
	value = gjson.Get(message, "data").Bool()
	assert.Assert(t, value == true)

	distancesChan <- mqttclient.MQTTClientSubscriberData{Topic: "test", Message: `{"data": {"value":` + fmt.Sprint(setpoint+(hysteresis+1)) + `}}`}
	message = <-contactorChan
	value = gjson.Get(message, "data").Bool()
	assert.Assert(t, value == false)

	distancesChan <- mqttclient.MQTTClientSubscriberData{Topic: "test", Message: `{"data": {"value":` + fmt.Sprint(setpoint-hysteresis) + `}}`}
	message = <-contactorChan
	value = gjson.Get(message, "data").Bool()
	assert.Assert(t, value == true)

	dC.Cancel()
	close(distancesChan)
	close(contactorChan)
}

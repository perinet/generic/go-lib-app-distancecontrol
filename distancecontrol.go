// Copyright (c) 2018-2022 Perinet GmbH
// All rights reserved

// This software is dual-licensed: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License version 3 as
// published by the Free Software Foundation. For the terms of this
// license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
// or contact us at https://perinet.io/contact/ when you want license
// this software under a commercial license.

// The appDistanceControl package implements the logic of the DistanceControl
// container. It is the application logic of Perinets Starter Kit Plus
// distributed application
package appDistanceControl

import (
	"encoding/json"

	"github.com/tidwall/gjson"
	"gitlab.com/perinet/generic/apiservice/mqttclient"
)

const (
	hysteresis = 5
)

type DistanceController struct {
	contactor      chan string                              // channel that distributes the calculated value to the contactor as JSON
	distances      chan mqttclient.MQTTClientSubscriberData // channel to receive various distances as JSON
	distance_limit uint32                                   // boundary to determine whether to enable or disable the contactor
	last           bool
}

type periNodeGpioState struct {
	Data bool `json:"data"`
}

// create a new DistanceController
func NewDistanceControl(limit uint32, contactor chan string, distances chan mqttclient.MQTTClientSubscriberData) *DistanceController {
	dC := DistanceController{}
	dC.distance_limit = uint32(limit)
	dC.last = false
	dC.contactor = contactor
	dC.distances = distances

	go dC.handleUpdate()

	return &dC
}

// cancel the DistanceController
func (dC *DistanceController) Cancel() {
}

func (dC *DistanceController) SetLimit(limit uint32) {
	dC.distance_limit = uint32(limit)
}

// calculate new contactor state
func (dC *DistanceController) calculate(distance uint32) bool {
	newLast := dC.last
	if !dC.last {
		if distance <= dC.distance_limit-hysteresis {
			newLast = true
		}
	} else {
		if distance >= dC.distance_limit+hysteresis {
			newLast = false
		}
	}
	dC.last = newLast
	return newLast
}

// handle update from distance sensor
func (dC *DistanceController) handleUpdate() {
	for message := range dC.distances {
		distance := gjson.Get(message.Message, "data.value").Float()
		message, _ := json.Marshal(periNodeGpioState{Data: dC.calculate(uint32(distance))})
		dC.contactor <- string(message)
	}
}
